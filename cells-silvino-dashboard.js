{
  const {
    html,
  } = Polymer;
  /**
    `<cells-silvino-dashboard>` Description.

    Example:

    ```html
    <cells-silvino-dashboard></cells-silvino-dashboard>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-silvino-dashboard | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsSilvinoDashboard extends Polymer.Element {

    static get is() {
      return 'cells-silvino-dashboard';
    }

    static get properties() {
      return {};
    }

    filtro(string){
      if(!string){
          return null;
      }else{
        // string = string.toLowerCase();//asgina el nombre del filtro
        return function(alumnos){
          let amigo = alumnos.name.toLowerCase();
          return (amigo.indexOf(string) != -1);
        };
      }
    }

    static get template() {
      return html `
      <style include="cells-silvino-dashboard-styles cells-silvino-dashboard-shared-styles"></style>
      <slot></slot>
      <iron-ajax
      auto
      url = "https://api.chucknorris.io/jokes/random"
      handle-as="json"
      last-response="{{infochuck}}">
      </iron-ajax>
   
        <input type="text" value="{{nombrefiltro::input}}" class="busqueda" placeholder="Buscar..."/>
        <br/><br/>
        <div class="card top-bar">
        [[infochuck.value]]<br/>
        <img src="[[infochuck.icon_url]]"/>
      </div>
      <cells-mocks-component alumnos="{{alumnos}}"></cells-mocks-component>
    <template is="dom-repeat" items="{{alumnos}}" filter="{{filtro(nombrefiltro)}}">
      <div class="card">
        <div class="circle"><img src="{{item.img}}"/></div>
        <p>Nombre:{{item.name}}</p> 
        <p>Dirección:{{item.address}}</p> 
        <p>Hobbies: {{item.hobbies}}</p>
      </div>
    </template>
    </div>
      `;
      
    }
    filtro(string){
      if(!string){
          return null;
      }else{
        string = string.toLowerCase();//asgina el nombre del filtro
        return function(alumnos){
          var amigo = alumnos.name.toLowerCase();
          return (amigo.indexOf(string) != -1);
        };
      }
    }
  }

  customElements.define(CellsSilvinoDashboard.is, CellsSilvinoDashboard);
}